try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

config = {
    'description': 'ChessLab Python',
    'author': 'Dejan Lekic',
    'url': 'URL to get it at.',
    'download_url': 'Where to download it.',
    'author_email': 'dejan.lekic@gmail.com',
    'version': '0.1',
#   'install_requires': [],
    'packages': ['ce_python'],
    'scripts': [],
    'name': 'ce_python'
}

setup(**config)
