#!/usr/bin/env python3
from ce_python.uci import *


# 24G: 24576 , 12G: 12288
#             number of threads  number of lines
#              hash size      |  |  depth (0 = infinite)
uci_config = UciConfig(12288, 6, 8, 22)
# for over-night runs do something like (it will go infinite):
# uci_config = UciConfig(24576, 8, 10, 0)
start_moves = ["e2e4", "c7c6"]
final_result = run_uci(uci_config, "stockfish", start_moves, verbosity=3)

best = final_result['best']

# If key 0 exists use it, otherwise use 1
kk = 0 if (0 in best) else 1
best_score_cp = best[kk].score_cp

print("Best line for [{}] is:".format(" ".join(start_moves)))
print("{} {}€: {}".format(kk, best[kk].score_cp, best[kk].pv))
print("Alternative lines:")
for alt_idx in range(2, uci_config.multipv + 1):
    if alt_idx in best:
        score_cp = best[alt_idx].score_cp
        if best_score_cp - score_cp < 30:
            print("{} {}€: {}".format(alt_idx, score_cp, best[alt_idx]))

print("-" * 100)
print(final_result)
