#!/usr/bin/env python3
from ce_python.uci import *


def main(args: list):
    # 24G: 24576 , 12G: 12288
    uci_config = UciConfig(24576, 8, 12, 0)
    start_moves = args[2:]
    start_dt = datetime.now()
    final_result = run_uci(uci_config, args[1], start_moves, verbosity=3)
    end_dt = datetime.now()
    secs = (end_dt - start_dt).total_seconds()
    best = final_result['best']
    best_score_cp = best[1].score_cp

    print("Best line for [{}] is:".format(" ".join(start_moves)))
    print("{} {}€: {}".format(1, best[1].score_cp, best[1].pv))
    print("Elapsed time: {} sec".format(secs))

    print("Alternative lines:")
    for alt_idx in range(2, uci_config.multipv):
        score_cp = best[alt_idx].score_cp
        if best_score_cp - score_cp < 30:
            print("{} {}€: {}".format(alt_idx, score_cp, best[alt_idx]))


if __name__ == "__main__":
    main(sys.argv)
