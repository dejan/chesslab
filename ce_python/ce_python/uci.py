from subprocess import Popen, PIPE
import sys
# from time import sleep
from datetime import datetime

"""
Run it with:
    bin/uci.py stockfish g1f3
    bin/uci.py amoeba g1f3
    bin/uci.py andscacs g1f3

An example from running an engine:

[b'info depth 22 seldepth 28 multipv 1 score cp 20 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv e7e5 d2d4 e5e4 c3c4 c7c6 c1f4 d7d5 e2e3 g8f6 b1d2 f8b4 a2a3 b4d2 d1d2 b8d7 g1e2 d5c4 e2c3 d7b6 f1e2 e8g8 e1g1 c8f5 h2h3 f6d5\n']
[b'info depth 22 seldepth 23 multipv 2 score cp 19 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv g8f6 d2d4 d7d5 c1f4 c8f5 e2e3 e7e6 g1f3 f8d6 f1d3 f5d3 d1d3 e8g8 e1g1 c7c5 f4d6 d8d6 b1d2 b8d7 f1e1 c5c4 d3c2 a8b8\n']
[b'info depth 22 seldepth 24 multipv 3 score cp 19 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv d7d5 g1f3 g8f6 d2d4 c8f5 c1f4 e7e6 e2e3 f8e7 b1d2 h7h6 f1e2 e8g8 e1g1 c7c5 d1b3 d8c8 c3c4 b8c6 d4c5 e7c5 c4d5 f6d5 f4g3\n']
[b'info depth 22 seldepth 34 multipv 4 score cp 8 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv e7e6 d2d4 g8f6 g1f3 d7d5 c1f4 f8d6 e2e3 c7c5 f1b5 b8c6 f4d6 d8d6 e1g1 e8g8 b1d2 c8d7 b5c6 d7c6 f3e5 f6d7 d2f3 c5d4 e5c6 b7c6 f3d4\n']
[b'info depth 22 seldepth 30 multipv 5 score cp -1 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv b8c6 d2d4 d7d5 g1f3 c8f5 e2e3 e7e6 f1b5 f8d6 c3c4 d5c4 b1d2 g8f6 d2c4 e8g8 b5c6 b7c6 e1g1 c6c5 b2b3 c5d4 e3d4 a8c8 f3e5\n']
[b'info depth 22 seldepth 25 multipv 6 score cp -8 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv b7b6 d2d4 g8f6 g1f3 e7e6 c1f4 d7d5 b1d2 f8d6 e2e3 c8b7 f1d3 e8g8 e1g1 c7c5 f3e5 h7h6 d3c2 b8c6 e5c6 b7c6 d2f3 d6f4 e3f4\n']
[b'info depth 22 seldepth 31 multipv 7 score cp -12 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv c7c5 e2e4 d7d5 e4d5 d8d5 g1f3 e7e5 d2d4 c5d4 c3d4 e5d4 d1d4 d5e6 c1e3 b8c6 f1b5 f8e7 e1g1 g8f6 b5c6 b7c6 f1e1 e8g8 e3f4 f8d8 e1e6 d8d4\n']
[b'info depth 22 seldepth 33 multipv 8 score cp -17 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv d7d6 d2d4 g8f6 c1f4 c8f5 g1f3 e7e6 e2e3 b8d7 f1d3 f5d3 d1d3 d6d5 e1g1 f8d6 b1d2 e8g8 f4d6 c7d6 e3e4 d5e4 d2e4 f6e4\n']
[b'info depth 22 seldepth 27 multipv 9 score cp -17 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv h7h6 e2e4 d7d5 e4d5 d8d5 d2d4 e7e5 g1f3 b8c6 b1d2 e5d4 f1c4 d5f5 e1g1 c8e6 f3d4 c6d4 c3d4 e8c8 c4e6 f7e6 d2c4 f5e4 c1e3 f8e7 c4e5 g8f6\n']
[b'info depth 22 seldepth 32 multipv 10 score cp -22 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv h7h5 g1f3 d7d5 d2d4 g8f6 c1f4 c8f5 e2e3 e7e6 f1d3 f5d3 d1d3 f8e7 b1d2 e8g8 h2h3 c7c5 d4c5 e7c5 f4g3 b8c6 e1g1 c5e7 b2b4\n']
[b'info depth 22 seldepth 32 multipv 11 score cp -23 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv a7a6 e2e4 d7d5 e4d5 d8d5 g1f3 e7e5 d2d4 b8c6 f1e2 e5e4 f3d2 e4e3 d2f3 e3f2 e1f2 g8f6 h1e1 f8e7 e2d3 d5h5 f2g1 e8g8 b1d2 c8f5 d3f5 h5f5 f3e5 e7d6 e5c6 b7c6 d2f3\n']
[b'info depth 22 seldepth 24 multipv 12 score cp -27 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv c7c6 e2e4 d7d5 e4d5 c6d5 d2d4 b8c6 g1f3 d8c7 f1d3 e7e6 e1g1 f8d6 f1e1 g8e7 b1a3 a7a6 a3c2 e8g8 h2h3 c8d7 d1e2\n']
[b'info depth 22 seldepth 26 multipv 13 score cp -31 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv f7f5 d2d4 e7e6 c1f4 f8e7 e2e3 g8f6 f1d3 e8g8 g1e2 c7c5 h2h3 b8c6 e1g1 b7b6 b1d2 c8b7 f4h2 f6d5 f1e1 d7d6 e2f4 d8d7 f4d5 e6d5\n']
[b'info depth 22 seldepth 35 multipv 14 score cp -40 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv g7g6 e2e4 g8f6 e4e5 f6h5 d2d4 d7d6 f1c4 d6e5 d1b3 e7e6 d4e5 b8d7 g1f3 c7c6 e1g1 d8c7 f1e1 b7b5 c4d3 d7c5 b3c2 c5d3 c2d3 c8b7 d3e2 a8d8 c1e3 c6c5 e2b5 b7c6 b5a6 f8e7 b1d2 e8g8\n']
[b'info depth 22 seldepth 31 multipv 15 score cp -54 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv a7a5 e2e4 d7d5 e4d5 d8d5 d2d4 e7e5 g1f3 b8c6 f1e2 g8f6 d4e5 d5d1 e2d1 f6d5 d1a4 d5b6 a4b5 f8e7 f3d4 c8d7 d4c6 b7c6 b5e2 e8g8 b1d2\n']
[b'info depth 22 seldepth 34 multipv 16 score cp -65 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv g8h6 e2e4 d7d5 e4d5 d8d5 g1f3 e7e5 d2d4 b8c6 f1e2 h6f5 d4e5 d5d1 e2d1 f8e7 b1d2 c8e6 e1g1 e8g8 d1b3 a8d8 d2e4 f5h4 f3h4 e7h4 f2f4 h4e7\n']
[b'bestmove e7e5 ponder d2d4\n']
"""  # noqa


class UciConfig:
    """
    A tiny class to hold various UCI configuration values.
    """

    def __init__(self,
                 hs: int=1024,
                 nt: int=1,
                 mpv: int=3,
                 dep: int=14) -> None:
        self.__hash_size = hs
        self.__number_of_threads = nt
        self.__multipv = mpv
        self.__depth = dep

    @property
    def hash_size(self):
        return self.__hash_size

    @hash_size.setter
    def hash_size(self, val):
        self.__hash_size = val

    @property
    def number_of_threads(self):
        return self.__number_of_threads

    @number_of_threads.setter
    def number_of_threads(self, val):
        self.__number_of_threads = val

    @property
    def multipv(self):
        return self.__multipv

    @multipv.setter
    def multipv(self, val):
        self.__multipv = val

    @property
    def depth(self):
        return self.__depth

    @depth.setter
    def depth(self, val):
        self.__depth = val


class UciInfo:

    def __init__(self) -> None:
        self.depth = 0
        self.seldepth = 0
        self.multipv = 0
        self.score_cp = 0
        self.score_upperbound = False
        self.score_lowerbound = False
        self.score_mate_in = 0
        self.nodes = 0
        self.nps = 0
        self.hashfull = 0
        self.tbhits = 0
        self.time = 0
        self.pv = []
        #  pv g8h6 e2e4 d7d5 e4d5 d8d5 g1f3 e7e5 d2d4 b8c6 f1e2 h6f5 d4e5 d5d1 e2d1 f8e7 b1d2
        #     c8e6 e1g1 e8g8 d1b3 a8d8 d2e4 f5h4 f3h4 e7h4 f2f4 h4e7\n']

    def __str__(self):
        return "No. %s  Depth: %s  Score: %s  PV: %s" % (self.multipv, self.depth,
                                                         self.score_cp, self.pv[:5])


def parse_uci_info(line: str) -> UciInfo:
    """
    UCI engine prints lots of info lines that look like:
    'info depth 22 seldepth 34 multipv 16 score cp -65 nodes 185362631 nps 7569529 hashfull 67 tbhits 0 time 24488 pv g8h6 e2e4 d7d5 e4d5 d8d5 g1f3 e7e5 d2d4 b8c6 f1e2 h6f5 d4e5 d5d1 e2d1 f8e7 b1d2 c8e6 e1g1 e8g8 d1b3 a8d8 d2e4 f5h4 f3h4 e7h4 f2f4 h4e7'
    This function parses such line and returns an UciInfo object
    """  # noqa

    parts = line.split()
    if len(parts) == 0:
        return None

    idx = 0  # type: int
    if parts[idx] == 'info':
        result = UciInfo()
        idx += 1
        while parts[idx] != 'pv':
            step = 0
            if parts[idx] == 'depth':
                result.depth = parts[idx + 1]
                step = 2
            if parts[idx] == 'seldepth':
                result.seldepth = parts[idx + 1]
                step = 2
            if parts[idx] == 'multipv':
                result.multipv = int(parts[idx + 1])
                step = 2
            if parts[idx] == 'nodes':
                result.nodes = parts[idx + 1]
                step = 2
            if parts[idx] == 'nps':
                result.nps = parts[idx + 1]
                step = 2
            if parts[idx] == 'hashfull':
                result.hashfull = parts[idx + 1]
                step = 2
            if parts[idx] == 'tbhits':
                result.tbhits = parts[idx + 1]
                step = 2
            if parts[idx] == 'time':
                result.tbhits = parts[idx + 1]
                step = 2
            if parts[idx] == 'score':
                step = 3
                if parts[idx + 1] == 'cp':
                    step = 3
                    result.score_cp = int(parts[idx + 2])
                    if parts[idx + 3] == 'upperbound':
                        result.score_upperbound = True
                        step = 4
                    if parts[idx + 3] == 'lowerbound':
                        result.score_lowerbound = True
                        step = 4
            idx += step
        result.pv = parts[idx + 1:]
        return result
    else:
        return None


def wait_for_uciok(uci_popen: Popen) -> list:
    result = []
    linestr = ''
    while not linestr.startswith('uciok'):
        linestr = uci_popen.stdout.readline().decode()
        result.append(linestr)
    return result


def all_same_depth(infos: dict) -> bool:
    sorted_indexes = list(infos.keys())
    sorted_indexes.sort()
    depth = infos[sorted_indexes[0]].depth
    for idx in infos.keys():
        elem = infos[idx]
        if depth != elem.depth:
            return False
    return True


def run_uci_loop(uci_popen: Popen, verbosity: int=0) -> dict:
    result = {'bestmove': None,
              'ponder': None,
              'best': None}

    skipped = 0
    keep_running = True
    infos = {}
    start_dt = datetime.utcnow()
    depth_start_dt = datetime.utcnow()
    depth_end_dt = datetime.utcnow()
    outstr = "##### Level {} finished in {:.3f} sec ##### total RT: {:.3f} sec ##### {}"
    while keep_running:
        line = uci_popen.stdout.readline()
        if line:
            linestr = line.decode()
            if linestr.startswith('bestmove'):
                # Parsing the bestmove line
                parts = linestr.split()
                if parts[0] == 'bestmove':
                    result['bestmove'] = parts[1]
                if len(parts) > 2:
                    if parts[2] == 'ponder':
                        result['ponder'] = parts[3]
                if verbosity > 1:
                    print(linestr)
                    print("Done!")
                keep_running = False

            if 'currmove' in linestr:
                skipped += 1
            else:
                skipped = 0
                if linestr.startswith('info'):
                    uci_info = parse_uci_info(linestr)
                    infos[uci_info.multipv] = uci_info
                    if all_same_depth(infos):
                        depth_end_dt = datetime.utcnow()
                        depth_elapsed = (depth_end_dt - depth_start_dt).total_seconds()
                        total_rt_sec = (depth_end_dt - start_dt).total_seconds()
                        if verbosity > 2:
                            for idx in infos.keys():
                                print(infos[idx])
                            sorted_indexes = list(infos.keys())
                            sorted_indexes.sort()
                            footer_str = outstr.format(infos[sorted_indexes[0]].depth,
                                                       depth_elapsed, total_rt_sec, depth_end_dt)
                            print(footer_str)
                        depth_start_dt = datetime.utcnow()
                else:
                    if verbosity > 1:
                        print(linestr)

    # final calculation of the total_rt_sec
    total_rt_sec = (depth_end_dt - start_dt).total_seconds()
    # put the total runtime (in seconds) into the result
    result['elapsed_time'] = total_rt_sec
    # Send the `quit` command to the UCI engine
    uci_popen.stdin.write(b"quit\n")
    uci_popen.stdin.flush()
    # Wait 10s for the process to terminate
    exit_status = uci_popen.wait(10.0)
    if verbosity > 2:
        print("Engine process ended with status: %d" % exit_status)
    result['best'] = infos
    return result


def run_uci(uci_config: UciConfig, chess_engine: str='stockfish', moves: list=None,
            verbosity: int=0):
    # executable with name in the chess_engine must be in the PATH
    ce_process = Popen([chess_engine], stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
    # ce_process.stdin.write('') # sends a space to the running process
    # ce_process.stdin.flush() # if the above isn't enough, try adding a flush
    ce_process.stdin.write(b"uci\n")
    ce_process.stdin.flush()
    out = wait_for_uciok(ce_process)

    # Debug
    if verbosity > 0:
        for line in out:
            print(line)

    if uci_config.multipv > 0:
        outstr = "setoption name MultiPV value " + str(uci_config.multipv) + "\n"
        ce_process.stdin.write(outstr.encode())
        if verbosity > 0:
            print(outstr)

    if uci_config.hash_size > 0:
        outstr = "setoption name Hash value " + str(uci_config.hash_size) + "\n"
        ce_process.stdin.write(outstr.encode())
        if verbosity > 0:
            print(outstr)

    if uci_config.hash_size > 0:
        outstr = "setoption name Hash value " + str(uci_config.hash_size) + "\n"
        ce_process.stdin.write(outstr.encode())
        if verbosity > 0:
            print(outstr)

    if uci_config.number_of_threads > 0:
        outstr2 = "setoption name Threads value " + str(uci_config.number_of_threads) + "\n"
        ce_process.stdin.write(outstr2.encode())
        if verbosity > 0:
            print(outstr2)

    ce_process.stdin.write("setoption name UCI_AnalyseMode value true\n".encode())
    ce_process.stdin.flush()

    # ucinewgame
    outstr = "ucinewgame\n\n"
    ce_process.stdin.write(outstr.encode())
    if verbosity > 0:
        print(outstr)

    # position
    pos = "position startpos\n\n"
    if moves and len(moves) > 0:
        pos = "position startpos moves " + " ".join(moves) + "\n"
    if verbosity > 0:
        print(pos)
    ce_process.stdin.write(pos.encode())

    if uci_config.depth > 0:
        outstr = "go depth " + str(uci_config.depth) + "\n"
    else:
        outstr = "go infinite\n"

    if verbosity > 0:
        print(outstr)

    ce_process.stdin.write(outstr.encode())
    ce_process.stdin.flush()

    # Run until the engine completes the process (unless it is the inifinite depth)
    # and return the final result
    return run_uci_loop(ce_process, verbosity)
