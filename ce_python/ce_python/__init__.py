whiteKing = 0
whiteQueen = 1
whiteRook = 2
whiteBishop = 3
whiteKnight = 4
whitePawn = 5
blackKing = 8
blackQueen = 9
blackRook = 10
blackBishop = 11
blackKnight = 12
blackPawn = 13

blackPieces = 'kqrbnp'
whitePieces = 'KQRBNP'

pieceToCharMap = dict([
                         (0, 'K'),
                         (1, 'Q'),
                         (2, 'R'),
                         (3, 'B'),
                         (4, 'N'),
                         (5, 'P'),
                         (8, 'k'),
                         (9, 'q'),
                         (10, 'r'),
                         (11, 'b'),
                         (12, 'n'),
                         (13, 'p')])

charToPieceMap = {
    'K': 0,
    'Q': 1,
    'R': 2,
    'B': 3,
    'N': 4,
    'P': 5,
    'k': 8,
    'q': 9,
    'r': 10,
    'b': 11,
    'n': 12,
    'p': 13
}


def emptyPosition():
    # [[' '] * 8] * 8 <-- does not work!
    return [[' '] * 8 for _ in range(8)]


def onlyKings():
    # An empty board with only two kings at their initial positions
    ret = emptyPosition()
    ret[0][4] = 'K'
    ret[7][4] = 'k'
    return ret


def initialPosition():
    ret = emptyPosition()
    ret[7] = ['r', 'n', 'b', 'q', 'k', 'b', 'n', 'r']
    ret[6] = ['p'] * 8
    ret[5] = [' '] * 8
    ret[4] = [' '] * 8
    ret[3] = [' '] * 8
    ret[2] = [' '] * 8
    ret[1] = ['P'] * 8
    ret[0] = ['R', 'N', 'B', 'Q', 'K', 'B', 'N', 'R']
    return ret


def piece2char(piece: int=5) -> str:
    return pieceToCharMap[piece]


def char2piece(char) -> int:
    return char2piece(char)


def move(file0, rank0, file1, rank1, promotion):
    return [chr(file0), chr(rank0), chr(file1), chr(rank1), chr(promotion)]


def printBoard(board: list=None):
    for i in range(7, -1, -1):
        rank = "".join(board[i])
        print(rank)


def getPieces(board: list=None, white: bool=True):
    res = []

    ser = 'kqrbnp'
    if white:
        ser = 'KQRBNP'

    for i in range(7, -1, -1):
        for j in range(7, -1, -1):
            if board[i][j] in ser:
                res.append((i, j))

    return res


def isWhite(piece: int=0):
    return piece in whitePieces


def isBlack(piece: int=0):
    return piece in blackPieces


def isKing(piece: int=0):
    return piece == 'k' or piece == 'K'
