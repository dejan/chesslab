import ce_python as ce


# print(ce.pieceToCharMap)
# print(ce.pieceToCharMap[1])
# print(ce.initialPosition())
print("initialPosition -------------")
ce.printBoard(ce.initialPosition())
print("emptyPosition -------------")
ce.printBoard(ce.emptyPosition())
print("onlyKings -------------")
ce.printBoard(ce.onlyKings())

simplePosition = ce.onlyKings()
simplePosition[1][3] = 'P'
simplePosition[1][4] = 'P'
simplePosition[1][5] = 'P'
simplePosition[6][4] = 'p'
print("simplePosition -------------")
ce.printBoard(simplePosition)

print(ce.getPieces(simplePosition))
print(ce.getPieces(simplePosition, False))


def moveKing(board: list=None, file: int=0, rank: int=0):
    me_white = ce.isWhite(board[rank][file])
    if me_white:
        myPieces = ce.whitePieces
        opponentPieces = ce.blackPieces
    else:
        myPieces = ce.blackPieces
        opponentPieces = ce.whitePieces

    allMoves = [(file - 1, rank - 1),
                (file - 1, rank),
                (file - 1, rank + 1),
                (file, rank + 1),
                (file + 1, rank + 1),
                (file + 1, rank),
                (file + 1, rank - 1),
                (file, rank - 1)]
    res1 = []
    for move in allMoves:
        # file
        if (0 <= move[0] <= 7) and (0 <= move[1] <= 7):
            res1.append(move)
    del allMoves

    res2 = []
    for move in res1:
        f = move[0]
        r = move[1]
        piece = board[r][f]
        if piece not in myPieces:
            res2.append(move)
    res = res2
    del res1
    return res


def availableMoves(board: list=None, file: int=0, rank: int=0) -> list:
    piece = board[rank][file]

    if ce.isKing(piece):
        return moveKing(board, file, rank)


print("availableMoves ---")
ce.printBoard(simplePosition)
print(availableMoves(simplePosition, 4, 0))
print(availableMoves(simplePosition, 4, 7))
"""
[a]  .
[a] b [c] d [e] f
"""
