package co.prj.ktce

/*
Tiny module which provides functions to deal with nibble values. (Mostly extracting and setting
nibble values in an Int object)
 */

// shl is available only for Int and Long...
/**
 *
 */
fun setNibble(num: Int, nibble: Byte, which: Int): Int {
    val shiftNibble: Int = (1 * nibble) shl (4 * which)
    val shiftMask: Int = 0x00_00_00_0F shl ( 4 * which)
    return (num and shiftMask.inv()) or shiftNibble
}

fun setNibble(num: Short, nibble: Byte, which: Byte): Short {
    val fw = 4 * which.toInt()
    val shiftNibble: Int = nibble.toInt() shl fw
    val shiftMask: Int = 0x00_0F shl fw
    return ((num.toInt() and shiftMask.inv()) or shiftNibble).toShort()
}

fun getNibble(data: Int, index: Byte): Byte {
    return ((data shr (index.toInt() shl 2)) and 0xF).toByte()
}

fun getNibble(data: Short, index: Byte): Byte {
    return (((1 * data) shr (index.toInt() shl 2)) and 0xF).toByte()
}
