package co.prj.ktce

typealias Position = Array<Array<Byte>>
typealias Piece = Byte
typealias Board = Array<ByteArray>

class IllegalMoveException(message: String) : Exception(message)
class UnknownPieceException(message: String) : Exception(message)

data class Move(val srcFile: Byte, val srcRank: Byte, val desFile: Byte, val desRank: Byte,
                val newPiece: Piece?=null)

object Pieces {
    val whiteKing = 0.toByte()
    val whiteQueen = 1.toByte()
    val whiteRook = 2.toByte()
    val whiteBishop = 3.toByte()
    val whiteKnight = 4.toByte()
    val whitePawn = 5.toByte()
    val blackKing = 8.toByte()
    val blackQueen = 9.toByte()
    val blackRook = 10.toByte()
    val blackBishop = 11.toByte()
    val blackKnight = 12.toByte()
    val blackPawn = 13.toByte()
}

object Locations {
    val a1 = 0x00
    val a2 = 0x01
    val a3 = 0x02
}

fun makeMove(a: Int, b: Int, c: Int, d: Int, e:Int?=null): Move =
    Move(a.toByte(), b.toByte(), c.toByte(), d.toByte(), e?.toByte())

fun pieceToString(piece: Piece): String {
    return when(piece) {
        0.toByte() -> "white king"     // 0000
        1.toByte() -> "white queen"    // 0001
        2.toByte() -> "white rook"     // 0010
        3.toByte() -> "white bishop"   // 0011
        4.toByte() -> "white knight"   // 0100
        5.toByte() -> "white pawn"     // 0101
        8.toByte() -> "black king"     // 1000
        9.toByte() -> "black queen"    // 1001
        10.toByte() -> "black rook"    // 1010
        11.toByte() -> "black bishop"  // 1011
        12.toByte() -> "black knight"  // 1100
        13.toByte() -> "black pawn"    // 1101
        else -> throw UnknownPieceException("Unknown piece value: $piece !")
    }
}

fun piece(piece: Piece): Char {
    when(piece) {
        0.toByte() -> return 'K'   // 0000
        1.toByte() -> return 'Q'   // 0001
        2.toByte() -> return 'R'   // 0010
        3.toByte() -> return 'B'   // 0011
        4.toByte() -> return 'N'   // 0100
        5.toByte() -> return 'P'   // 0101
        8.toByte() -> return 'k'   // 1000
        9.toByte() -> return 'q'   // 1001
        10.toByte() -> return 'r'  // 1010
        11.toByte() -> return 'b'  // 1011
        12.toByte() -> return 'k'  // 1100
        13.toByte() -> return 'p'  // 1101
        else -> throw UnknownPieceException("Unknown piece value: $piece !")
    }
}

fun fileToChar(file: Byte): Char {
    return when(file) {
        1.toByte() -> 'a'
        2.toByte() -> 'b'
        3.toByte() -> 'c'
        4.toByte() -> 'd'
        5.toByte() -> 'e'
        6.toByte() -> 'f'
        7.toByte() -> 'g'
        8.toByte() -> 'h'
        else -> throw IllegalMoveException("Invalid file number $file !")
    }
}

fun rankToChar(rank: Int): Char {
    return when (rank) {
        1 -> '1'   // 0000
        2 -> '2'   // 0001
        3 -> '3'   // 0010
        4 -> '4'   // 0011
        5 -> '5'   // 0100
        6 -> '6'   // 0101
        7 -> '7'   // 0110
        8 -> '8'   // 0111
        else -> throw IllegalMoveException("Invalid file number $rank !")
    }
}

fun rankToChar(rank: Byte): Char = rankToChar(rank.toInt())

fun move(move: Move): String {
    val promotion: String =
        when(move.newPiece) {
            null -> ""
            else -> "=" + piece(move.newPiece)
        }
    return "" + fileToChar(move.srcFile) + rankToChar(move.srcRank) + '-' +
            fileToChar(move.desFile) + rankToChar(move.desRank) + promotion
}

fun algebraic(move: Move, piece: Piece): String {
    val promotion: String =
        when(move.newPiece) {
            null -> ""
            else -> "=" + piece(move.newPiece)
        }
    return "" + fileToChar(move.srcFile) + rankToChar(move.srcRank) + '-' +
            fileToChar(move.desFile) + rankToChar(move.desRank) + promotion
}

fun availableMoves(position: Position, white: Boolean=true, plyCount: Byte): Array<Short> {
    return arrayOf(1.toShort(), 2.toShort(), 3.toShort())
}

/**
 * A function that returns a starting chess board (with all pieces at their initial places).
 *
 * @return Board A new Board object (essentially a byte[8][8] array)
 */
fun board(): Board {
    val board: Board = Array(8, {ByteArray(8)})

    // White
    board[0][0] = Pieces.whiteRook
    board[0][1] = Pieces.whiteKnight
    board[0][2] = Pieces.whiteBishop
    board[0][3] = Pieces.whiteQueen
    board[0][4] = Pieces.whiteKing
    board[0][5] = Pieces.whiteBishop
    board[0][6] = Pieces.whiteKnight
    board[0][7] = Pieces.whiteRook
    for (i in 0..7)
        board[1][i] = Pieces.whitePawn

    // Empty squares
    for (i in 2..5)
        for (j in 0..7)
            board[i][j] = 0xFF.toByte()

    // Black
    for (i in 0..7)
        board[6][i] = Pieces.blackPawn

    board[7][0] = Pieces.blackRook
    board[7][1] = Pieces.blackKnight
    board[7][2] = Pieces.blackBishop
    board[7][3] = Pieces.blackQueen
    board[7][4] = Pieces.blackKing
    board[7][5] = Pieces.blackBishop
    board[7][6] = Pieces.blackKnight
    board[7][7] = Pieces.blackRook

    return board
}

fun printBoard(board: Board) {
    for (i in (0..7).reversed()) {
        var row = "" + rankToChar(i + 1) + " "
        for (j in 0..7) {
            if (board[i][j] == 0xFF.toByte()) {
                row += '.'
            } else {
                row += piece(board[i][j])
            }
        }
        println(row)
    }
    println("  abcdefgh")
}

fun main(vararg args: String) {
    println("hello")
    val x = setNibble(0x00112233, 0x0f, 1)
    println(java.lang.Integer.toHexString(x))
    println(getNibble(x, 2))
    val move = makeMove(7, 1, 6, 3)
    println(move(move))
    val board = board()
    printBoard(board)
}
