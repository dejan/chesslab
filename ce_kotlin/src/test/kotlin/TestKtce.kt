package test.kotlin

import org.junit.Assert
import org.junit.Test
import co.prj.ktce.getNibble

class TestKtce {
    @Test fun testGetNibble() {
        val data = 0x12345678
        val x0 = getNibble(data, 0)
        Assert.assertEquals(x0, 8.toByte())
        val x1 = getNibble(data, 1)
        Assert.assertEquals(x1, 7.toByte())
        val x2 = getNibble(data, 2)
        Assert.assertEquals(x2, 6.toByte())
        val x3 = getNibble(data, 3)
        Assert.assertEquals(x3, 5.toByte())
        val x4 = getNibble(data, 4)
        Assert.assertEquals(x4, 4.toByte())
        val x5 = getNibble(data, 5)
        Assert.assertEquals(x5, 3.toByte())
        val x6 = getNibble(data, 6)
        Assert.assertEquals(x6, 2.toByte())
        val x7 = getNibble(data, 7)
        Assert.assertEquals(x7, 1.toByte())
    }
}