package co.prj.scess

import org.scalatest._

class ScessTest extends FlatSpec with Matchers {

  "return42" should "return 42" in {
    val obj = new Scess
    obj.return42() should be(42)
  }
}