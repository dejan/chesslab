package co.prj.scess

class Scess {
  def return42(): Int = 42
} // Scess class

object Scess {
  def main(args: Array[String]): Unit = {
    val obj = new Scess
    println(obj.return42())
    println("hello")
  }
} // Scess object
